# Code Book
## Getting and Cleaning Data Course Project
##### Lucius Bynum, February 2017

## The Data
The original data set consists of data collected from accelerometers from the Samsung Galaxy S smartphone. A full description is available at the site where the data were obtained: 

http://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones.

Detailed descriptions of how the data were collected and compiled can be found at the site where the data were obtained. 

## Variables
Below is an excerpt from the original documentation describing the signals collected and their units.

>The features selected for this database come from the accelerometer and gyroscope 3-axial raw signals tAcc-XYZ and tGyro-XYZ. These time domain signals (prefix 't' to denote time) were captured at a constant rate of 50 Hz. Then they were filtered using a median filter and a 3rd order low pass Butterworth filter with a corner frequency of 20 Hz to remove noise. Similarly, the acceleration signal was then separated into body and gravity acceleration signals (tBodyAcc-XYZ and tGravityAcc-XYZ) using another low pass Butterworth filter with a corner frequency of 0.3 Hz.

>Subsequently, the body linear acceleration and angular velocity were derived in time to obtain Jerk signals (tBodyAccJerk-XYZ and tBodyGyroJerk-XYZ). Also the magnitude of these three-dimensional signals were calculated using the Euclidean norm (tBodyAccMag, tGravityAccMag, tBodyAccJerkMag, tBodyGyroMag, tBodyGyroJerkMag). 

>Finally a Fast Fourier Transform (FFT) was applied to some of these signals producing fBodyAcc-XYZ, fBodyAccJerk-XYZ, fBodyGyro-XYZ, fBodyAccJerkMag, fBodyGyroMag, fBodyGyroJerkMag. (Note the 'f' to indicate frequency domain signals). 

>These signals were used to estimate variables of the feature vector for each pattern:  
'-XYZ' is used to denote 3-axial signals in the X, Y and Z directions.

>* BodyAcc-XYZ
* GravityAcc-XYZ
* BodyAccJerk-XYZ
* BodyGyro-XYZ
* BodyGyroJerk-XYZ
* BodyAccMag
* GravityAccMag
* BodyAccJerkMag
* BodyGyroMag
* BodyGyroJerkMag
* BodyAcc-XYZ
* BodyAccJerk-XYZ
* BodyGyro-XYZ
* BodyAccMag
* BodyAccJerkMag
* BodyGyroMag
* BodyGyroJerkMag

A large set of variables were estimated from these signals and are available in the original data set. This data set extracts only the measurements on the mean and standard deviation for each signal.

### Identifying Variables
**subject**: the experiments were carried out with a group of 30 volunteers within an age bracket of 19-48 years. Each number corresponds to a different volunteer.

**activity**: each person performed six activities. This variable is a factor variable for each of those activities in the following order: walking, walkingUpstairs, walkingDownstairs, sitting, standing, laying. These factors have values 1 through 6 respectively.

### Signal Variables
All signal variables are described above and in the original data set documentation. This data set contains the following two sets of variables:

Mean for each signal:

* **tBodyAcc(-XYZ)Mean**
* **tGravityAcc(-XYZ)Mean**
* **tBodyAccJerk(-XYZ)Mean**
* **tBodyGyro(-XYZ)Mean**
* **tBodyGyroJerk(-XYZ)Mean**
* **tBodyAccMagMean**
* **tGravityAccMagMean**
* **tBodyAccJerkMagMean**
* **tBodyGyroMagMean**
* **tBodyGyroJerkMagMean**
* **fBodyAcc(-XYZ)Mean**
* **fBodyAccJerk(-XYZ)Mean**
* **fBodyGyro(-XYZ)Mean**
* **fBodyAccMagMean**
* **fBodyAccJerkMagMean**
* **fBodyGyroMagMean**
* **fBodyGyroJerkMagMean**

Standard deviation for each signal:
* **tBodyAcc(-XYZ)Std**
* **tGravityAcc(-XYZ)Std**
* **tBodyAccJerk(-XYZ)Std**
* **tBodyGyro(-XYZ)Std**
* **tBodyGyroJerk(-XYZ)Std**
* **tBodyAccMagStd**
* **tGravityAccMagStd**
* **BodyAccJerkMagStd**
* **BodyGyroMagStd**
* **BodyGyroJerkMagStd**
* **BodyAcc(-XYZ)Std**
* **BodyAccJerk(-XYZ)Std**
* **BodyGyro(-XYZ)Std**
* **BodyAccMagStd**
* **BodyAccJerkMagStd**
* **BodyGyroMagStd**
* **BodyGyroJerkMagStd**

## Transformations Performed
Transformations performed on the data include:

1. Merging the training and the test sets to create one data set.
2. Extracting only the measurements on the mean and standard deviation for each measurement.
3. Using descriptive activity names to name the activities in the data set
4. Labeling the data set with descriptive variable names.
5. From the data set in step 4, creating a second, independent tidy data set with the average of each variable for each activity and each subject.