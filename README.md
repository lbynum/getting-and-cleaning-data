# Getting and Cleaning Data Course Project
##### Lucius Bynum, February 2017

This repository contains a script 'run_analysis.R' that extracts and tidies data for human activity recognition via Samsung Galaxy S smartphone accelerometer data originally obtained from:

http://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones

Starting with original downloaded dataset, this script merges the mean and standard deviation measurements from the training and test sets into one tidy data set (Note: data should be at the local path "./data/UCI HAR Dataset/"). It does the following:

1. Merges the training and the test sets to create one data set.
2. Extracts only the measurements on the mean and standard deviation for each measurement.
3. Uses descriptive activity names to name the activities in the data set
4. Labels the data set with descriptive variable names, storing the result in the variable 'mergedDataFrame'.
5. From the data set in step 4, creates a second, independent tidy data set with the average of each variable for each activity and each subject
6. Writes the data set created in step 5 to the file 'tidy.txt'.
