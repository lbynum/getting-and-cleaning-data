# Lucius Bynum
# Feb. 2017
# Coursera - Getting and Cleaning Data
# Description: script for human activity recognition via Samsung Galaxy S
#   smartphone accelerometer data. Starting with original downloaded dataset,
#   this script merges the training and test sets into one tidy data set
# Note: data should be at the local path "./data/UCI HAR Dataset/"

library(dplyr)

# ensure data folder has been downloaded
dataFolderPath = "./data/UCI HAR Dataset"
if(!dir.exists(dataFolderPath)) 
  stop("no directory found at ./data/UCI HAR Dataset")


# extract feature factors corresponding to mean and standard deviation for each 
#  measurement
featuresOfInterest = read.table("./data/UCI HAR Dataset/features.txt") %>%
                     .[,2]
featureIndexes = grep("mean\\(\\)|std\\(\\)", featuresOfInterest)
featuresOfInterest = featuresOfInterest[featureIndexes]

# read measurements for those features
test = read.table("./data/UCI HAR Dataset/test/X_test.txt", header = FALSE)[featureIndexes]
train = read.table("./data/UCI HAR Dataset/train/X_train.txt", header = FALSE)[featureIndexes]

# clean up feature labels to camel case
levels(featuresOfInterest) <- levels(featuresOfInterest) %>%
                              gsub("-m","M", .) %>%
                              gsub("-s","S", .) %>%
                              gsub("-","", .) %>%
                              gsub("\\(\\)","", .)

# rename columns in test and train data frames
names(test) = as.character(featuresOfInterest)
names(train) = as.character(featuresOfInterest)

# read activity table and separate into levels and labels
activityTable = read.table("./data/UCI HAR Dataset/activity_labels.txt", header = FALSE)
activityLevels = activityTable[,1]
activityLabels = as.character(activityTable[,2])

# read corresponding activity labels for measurements of interest
trainLabels = scan("./data/UCI HAR Dataset/train/y_train.txt", what = numeric(), sep = "\n")
testLabels = scan("./data/UCI HAR Dataset/test/y_test.txt", what = numeric(), sep = "\n")

# factorize activity labels
trainLabels = factor(trainLabels, levels = activityLevels, labels = activityLabels)
testLabels = factor(testLabels, levels = activityLevels, labels = activityLabels)

# read test and train subjects
trainSubjects = scan("./data/UCI HAR Dataset/train/subject_train.txt", what = numeric(), sep = "\n")
testSubjects = scan("./data/UCI HAR Dataset/test/subject_test.txt", what = numeric(), sep = "\n")

# add subjects and activity labels to train and test data frames
train = cbind(subject = trainSubjects, activity = trainLabels, train)
test = cbind(subject = testSubjects, activity = testLabels, test)

# merge data sets into dplyr data frame
mergedDataFrame = tbl_df(rbind(train, test))

# arrange by subject and activity
mergedDataFrame = arrange(mergedDataFrame, subject, activity)

# create summary version with the average of each variable for each activity and 
#  each subject
summaryDataFrame = mergedDataFrame %>%
                   group_by(subject, activity) %>%
                   summarize_each(funs(mean))

# write tidy data table to file
write.table(summaryDataFrame, file = "tidy.txt", row.names = FALSE)



